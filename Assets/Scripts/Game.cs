﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
/*
namespace Piy
{
    public class GFG
    {

        // addstr contain three parameters 
        public static void addstr(string s1, string s2 = "ште", string s3 = "int")
        {
            string result = s1 + s2 + s3;
            Console.WriteLine("Final string is: " + result);
        }

        // Main Method 
        static public void Main()
        {
            // calling the static method with named  
            // parameters without any order 
            addstr("Geeks", s3: "Geeks");

        }
    }
}
*/
public class Game : MonoBehaviour
{   /// The width of the grid...
    public static int gridWidth = 10;
    /// The height of the grid...
    public static int gridHeight = 20;

    /// The grid...
    public static Transform[,] grid = new Transform[gridWidth, gridHeight];

    public int scoreOneLine = 40;
    public int scoreTwoLine = 100;
    public int scoreThreeLine = 300;
    public int scoreFourLine = 1200;
    public GameObject boxPrototype;
    public ParticleSystem Effects;

    public AudioClip clearedLineSound;

    public Text hud_score;

    private int numberOfRowsThisTurn = 0;

    private AudioSource audioSource;

    public static int currentScore;

    void Start()
    {
        SpawnNextTetromino();

        audioSource = GetComponent<AudioSource>();

        for (int i = 0; i <= 10; ++i)
        {
            CreateBorder(-1 + i, -1);
        }

        for (int i = 0; i <= 30; ++i)
        {
            CreateBorder(-1, -1 + i);
        }

        for (int i = 0; i <= 30; ++i)
        {
            CreateBorder(10, -1 + i);
        }
    }

    void CreateBorder(int x, int y, int z = 0)
    {
        GameObject box = Object.Instantiate(boxPrototype);
        box.transform.position = new Vector3(x, y, z);
    }

    void Update()
    {
        UpdateScore();
        UpdateUI();

        if (Input.GetKeyDown(KeyCode.UpArrow) == true)
        { 
            Effects.Play();
        }
        if (Input.GetKeyUp(KeyCode.UpArrow) == true)
        {
            Effects.Stop();
        }
    }

    public void UpdateUI()
    {
        hud_score.text = currentScore.ToString();
    }

    public void UpdateScore()
    {
        if (numberOfRowsThisTurn > 0)
        {
            if (numberOfRowsThisTurn == 1)
            {
                ClearedOneLine();
            }
            else if (numberOfRowsThisTurn == 2)
                {
                    ClearedTwoLines();
                }
                else if (numberOfRowsThisTurn == 3)
                    {
                        ClearedThreeLines();
                    }
                    else if (numberOfRowsThisTurn == 4)
                        {
                            ClearedFourLines();
                        }

            numberOfRowsThisTurn = 0;
            PlayLineClearSound();
        }
    }

    public void ClearedOneLine()
    {
        currentScore += scoreOneLine;
    }

    public void ClearedTwoLines()
    {
        currentScore += scoreTwoLine;
    }

    public void ClearedThreeLines()
    {
        currentScore += scoreThreeLine;
    }

    public void ClearedFourLines()
    {
        currentScore += scoreFourLine;
    }
    
    public void PlayLineClearSound()
    {
        audioSource.PlayOneShot(clearedLineSound);
    }

    /// Check the is above grid...
    public bool CheckIsAboveGrid (Tetromino tetromino)
    {
        for(int x = 0; x < gridWidth; ++x)
        {
            foreach (Transform mino in tetromino.transform)
            {
                Vector2 pos = Round(mino.position);
                if (pos.y > gridHeight - 1)
                {
                    return true;
                }
            }
        }
        return false;
    }
    /// Check the is inside grid...
    public bool CheckIsInsideGrid(Vector2 pos)
    {
        //- Check if the pos that was specified is within the boudaries of the grid.
        return ((int)pos.x >= 0 && (int)pos.x < gridWidth && (int)pos.y >= 0);
    }
    /// Defermines if there is a full row at the specified y...
    public bool IsFullRowAt(int y)
    {
        //-The parameter y, is the row we will iterate over in the grid array to check each x position for a transform.
        for (int x = 0; x < gridWidth; ++x)
        {
            //- If we find a position that returns NULL instead of a transform, then we know that the row is not full.
            if (grid[x, y] == null)
            {
                //- So we return false
                return false;
            }
        }
        //- Since we found a full row, we increment the full row variable.
        numberOfRowsThisTurn++;

        //- If we iterated over the entire loop and didn't encounter any NULL positions, then we return true;
        return true;
    }
    /// Deletes the mino at y...
    public void DeleteMinoAt(int y)
    {
        //- The parameter y, is the row we will iterate over in the grid array.
        for(int x = 0; x < gridWidth; ++x)
        {
            //- We destroy the gameObject of each transform at the current iteration of x, y
            Destroy(grid[x, y].gameObject);
            //- We then set the x, y location in the grid array to null 
            grid[x, y] = null;
        }
    }
    /// Rows down...
    public void MoveRowDown(int y)
    {
        //- We iterate over each mino in the row of the y coordinate.
        for(int x = 0; x < gridWidth; ++x)
        {
            //- We check if the current x and y in the grid array does not equal null
            if (grid[x, y] != null)
            {
                //- If it doesn't then we have to set the current transform one pposition below in the grid
                grid[x, y - 1] = grid[x, y];
                //- We then set the current transform to null
                grid[x, y] = null;
                //- and then we adjust the position of the sprite to move down by 1
                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }
    /// Rows down all...
    public void MoveAllRowsDown(int y)
    {
        //- The y coordinate is the row above the row that was just deleted. 
        //- So we iterate over each row starting with that one all the way to the height of the grid
        for (int i = y; i < gridHeight; ++i)
        {
            //- We then call Move Row down for each iteration
            MoveRowDown(i);
        }
    }
    /// Deletes the row...
    public void DeleteRow()
    {
        //- After receiving a call from the Tetromino class, we iterate over all the y positions to gridHeight
        for ( int y = 0; y < gridHeight; ++y)
        {
            //- At each iteration we check if the row is full
            if (IsFullRowAt(y))
            {
                //- If it is, we call deleteminoat and pass in the current y
                DeleteMinoAt(y);
                MoveAllRowsDown(y + 1);
                --y;
            }
        }
    }
    /// Updates the grid...
    public void UpdateGrid(Tetromino tetromino)
    {
        //- In this method, we update our grid array. 
        //- We do this by starting a for loop that iterates over all the grid rows starting at 0 
        for (int y = 0; y < gridHeight; ++y)
        {
            //- For each row, we iterate over each individual x coordinate that represents a spot on the grid where a mino could be 
            for (int x = 0; x < gridWidth; ++x)
            {
                if (grid[x, y] != null)
                {
                    if (grid[x, y].parent == tetromino.transform)
                    {
                        grid[x, y] = null;
                    }
                }
            }
        }
        foreach (Transform mino in tetromino.transform)
        {
            Vector2 pos = Round(mino.position);
            if (pos.y < gridHeight)
            {
                grid[(int)pos.x, (int)pos.y] = mino;
            }
        }
    }
    /// Gets the transform at grid position...
    public Transform GetTransformAtGridPosition(Vector2 pos)
    {
        //- Because we instantiate the tetrominos abovethe height of the grid which is not part of the array,
        //- we have return null instead of attempting to return a transform that doesn't exist
        if (pos.y > gridHeight - 1)
        {
            return null;
        }else
        {
            return grid[(int)pos.x,(int)pos.y];
        }
    }
    /// Spawns the next tetromino... 
    public void SpawnNextTetromino()
    {
        //- Spawns another tetromino from the resources assets folder using the GetRandomTetromino method to select a random prefab
        GameObject nextTetromino = (GameObject)Instantiate(Resources.Load(GetRandomTetromino(), 
                                    typeof(GameObject)), new Vector2(5.0f, 20.0f), Quaternion.identity);
    }
    /// Round the specified pos...
    public Vector2 Round(Vector2 pos)
    {
        //- A simple helper method that will round the x and y positions
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }
    /// Gets a random tetromino... 
    string GetRandomTetromino()
    {
        //- Here we use Random.Range to generate a random number between 1 and 8(meaning we get back 1, 2, 3, 4, 5, 6 or 7)
        int randomTetromino = Random.Range(1, 8);
        //- We then create a string variable with an identifier of rendomTetrominoName and assign it a default value of Tetromino_T
        string randomTetrominoName = "Prefabs/Tetromino_T";
        //- We then use the switch condition to test for each possible random number case 
        //- and then assign a different prefab name to the randomTetrominoName veriable
        switch (randomTetromino)
        {
            case 1:
                randomTetrominoName = "Prefabs/Tetromino_T";
                break;
            case 2:
                randomTetrominoName = "Prefabs/Tetromino_Long";
                break;
            case 3:
                randomTetrominoName = "Prefabs/Tetromino_Square";
                break;
            case 4:
                randomTetrominoName = "Prefabs/Tetromino_J";
                break;
            case 5:
                randomTetrominoName = "Prefabs/Tetromino_L";
                break;
            case 6:
                randomTetrominoName = "Prefabs/Tetromino_S";
                break;
            case 7:
                randomTetrominoName = "Prefabs/Tetromino_Z";
                break;
        }
        return randomTetrominoName;
    }
    /// Loads the GameOver Scene...
    public void GameOver()
    {
        Application.LoadLevel("GameOver");
    }

}
/*public class Game : MonoBehaviour {

    public Element m_element;

    public const int SizeX = 10;
    public const int SizeY = 20;

    public GameObject m_prototype;

    public int direction = -1;
    public bool move = false;
    public Transform m_borderGO;
    float time = 0;
   
    Element.Cube[,] m_field2 = new Element.Cube[SizeX, SizeY];

    void Update()
    {
        //if (m_field == null)
        //{
        //    m_field = new bool[m_sizeX, m_sizeY];
        //}

        // C#
        //if (move)
        float border = m_borderGO.position.y;

        //if (transform.position.y > border)
        //{
        //    transform.position += Vector3.up * 0.5f * direction * Time.deltaTime;
        //}

        if (m_element == null)
        {
            m_element = Instantiate(m_prototype).GetComponent<Element>();
            m_element.m_posY = SizeY - 1;
            m_element.m_posX = SizeX / 2;
            m_element.Populate();
        }

        if (Input.GetKeyDown(KeyCode.D) && m_element.m_posX < SizeX - 1 && m_field2[m_element.m_posX + 1, m_element.m_posY] == null)
        {
            m_element.m_posX++;
            m_element.Populate();
        }

        if (Input.GetKeyDown(KeyCode.A) && m_element.m_posX > 0 && m_field2[m_element.m_posX - 1, m_element.m_posY] == null)
        {
            m_element.m_posX--;
            m_element.Populate();
        }

        if (Input.GetKeyDown(KeyCode.S) && m_element.m_posY > 0 && m_field2[m_element.m_posX, m_element.m_posY - 1] == null)
        {
            m_element.m_posY--;
            m_element.Populate();
        }

        if (time >= 1)
        {
            if (m_element.m_posY > 0 && m_field2[m_element.m_posX, m_element.m_posY - 1] == null)
            {
                m_element.m_posY--;
                m_element.Populate();
            }
            else
            {
      
                m_field2[m_element.m_posX, m_element.m_posY] = m_element.m_cube;
                m_element.m_cube.ConvertToGlobalPos(m_element);


                // Проверять заполненные строки 

                // удаление кубика
                //m_field2[1,3].Destroy();
                //m_field2[1,3] = null;

                // смещенеи вниз одного кубика элемента
                //m_field2[1, 3] = m_field2[1, 4];
                //m_field2[1, 4] = null;
                //m_field2[1,3].m_posY--;
               // m_field2[1,3].Populate();

                m_element = null;
            }
            time = 0;
        }
        time += Time.deltaTime;
    }
} */
