﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetromino : MonoBehaviour
{
    float fall = 0;              //- Countdown timer for fall speed
    public float fallSpeed = 1;              
    public bool allowRotation;   //- We use this to specify if we want to allow the tetromino to rotate
    public bool limitRotation;   //- This is used to limit the rotation of the tetromino to a 90 /-90 rotation.(To/From)
    public string prefabName;

    public AudioClip moveSound;    //- Sound for when tetromino is moved
    public AudioClip rotateSound;  //- Sound for when tetromino is rotated
    public AudioClip landSound;    //- Sound for when tetromino is lands

    private float continuousVerticalSpeed = 0.05f;
    private float continuousHorizontalSpeed = 0.01f;

    private float verticalTimer = 0;
    private float horizontalTimer = 0;

    public int individualScore = 100;

    private AudioSource audioSource;

    private float individualScoreTime;

    /// Use this for initialization
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    

    /// <summary>
    /// Update this instance.
    /// </summary>
    void Update() {
        //- We need to call the CheckUserInput method so that the tetromino will be properly positioned
        CheckUserInput();

        UpdateIndividualScore();
    }

    void UpdateIndividualScore()
    {
        if (individualScoreTime < 1)
        {
            individualScoreTime += Time.deltaTime;
        }else
        {
            individualScoreTime = 0;
            individualScore = Mathf.Max(individualScore - 10, 0);
        }
    }
    
    /// <summary>
    /// Checks the user input.
    /// </summary>
    void CheckUserInput()
    {
        //- This method checks the keys that the player can press to manipulate the position of the tetromino.
        //- The options here will be left, right, up and down.
        //- Left and right will move the tetromino one unit to the left or right
        //- Down will move the tetromino 1 unit down
        //- Up will rotate the tetromino
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //- First we attempt to move the tetromino to the right
            transform.position += new Vector3(1, 0, 0);
            //- We then check if the tetromino is at a valid position
            if (CheckIsValidPosition())
            {
                //- If it is, we then call the UpdateGrid method which records this tetromino new position 
                FindObjectOfType<Game>().UpdateGrid(this);
                PlayMoveAudio();
            }else
            {
                //- If it isn't we move the tetromino back to the left 
                transform.position += new Vector3(-1, 0, 0);
            }

        }else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {

            transform.position += new Vector3(-1, 0, 0);
            if (CheckIsValidPosition())
            {
                FindObjectOfType<Game>().UpdateGrid(this);
                PlayMoveAudio();
            }
            else
            {

                transform.position += new Vector3(1, 0, 0);
            }

        }else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //- The up key was pressed, lets first check if the tetromino is allowed to rorate
            if (allowRotation)
            {
                //- If it is, we need to check if the rotation is limited to just back and forth
                if (limitRotation)
                {
                    //- If it is, we need to check what the current rotation is
                    if (transform.rotation.eulerAngles.z >= 90)
                    {
                        //- If it is at 90 then we know it was already rotated, so we rotate it back by -90
                        transform.Rotate(0, 0, -90);
                    }else
                    {
                        //- If it isn't, then we rotate it to 90
                        transform.Rotate(0, 0, 90);
                    }
                }else
                {
                    //- If it isn't, we rotate it to 90
                    transform.Rotate(0, 0, 90);
                }
                    //- Now we check if the tetromino is at a valid position after attempting a rotation
                    if (CheckIsValidPosition())
                    {
                        //- If the position is valid, we update the grid
                        FindObjectOfType<Game>().UpdateGrid(this);
                    PlayRotateAudio();
                    }else
                    {
                        //- If it isn't, we rotate it back -90
                        if (transform.rotation.eulerAngles.z >= 90)
                        {
                            transform.Rotate(0, 0, -90);
                        }else
                        {
                            transform.Rotate(0, 0, 90);
                        }
                    }
            }
        }else if (Input.GetKeyDown(KeyCode.DownArrow) || Time.time - fall >= fallSpeed)
        {
            transform.position += new Vector3(0, -1, 0);
            if (CheckIsValidPosition())
            {
                FindObjectOfType<Game>().UpdateGrid(this);
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    PlayMoveAudio();
                }
            }
            else
            {
                transform.position += new Vector3(0, 1, 0);
                FindObjectOfType<Game>().DeleteRow();
                //- Check if there are any minos above the grid
                if (FindObjectOfType<Game>().CheckIsAboveGrid(this))
                {
                    FindObjectOfType<Game>().GameOver();
                }
                //- Spawn the next piece
                PlayLandAudio();
                FindObjectOfType<Game>().SpawnNextTetromino();
                Game.currentScore += individualScore;
                enabled = false;
            }
            fall = Time.time;
        }

    }
    /// <summary>
    /// Plays audio clip when tetromino is moved left, right or down.
    /// </summary>
    void PlayMoveAudio()
    {
        audioSource.PlayOneShot(moveSound);
    }
    /// <summary>
    /// Plays audio cli when tetromino is rotated.
    /// </summary>
    void PlayRotateAudio()
    {
        audioSource.PlayOneShot(rotateSound);
    }
    /// <summary>
    /// Plays audio clip when tetromino lands.
    /// </summary>
    void PlayLandAudio()
    {
        audioSource.PlayOneShot(landSound);
    }

    /// <summary>
    /// Checks the is valid position.
    /// </summary>
    /// <returns><c> true </c>, if is valid position was checked, <c> false <c/> otherwise.</returns>
    bool CheckIsValidPosition(){

            foreach (Transform mino in transform) {

                Vector2 pos = FindObjectOfType<Game>().Round (mino.position);
                if (FindObjectOfType<Game>().CheckIsInsideGrid (pos) == false) {

                    return false;
                }
                if (FindObjectOfType<Game>().GetTransformAtGridPosition(pos) != null && FindObjectOfType<Game>().GetTransformAtGridPosition(pos).parent != transform)
                {
                    return false;
                }
            }
            return true;
    }
    
}
